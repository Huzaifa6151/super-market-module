include irvine32.inc

.data

	a byte "************************************************************************************************************************",0
	b byte "***                                                                                                                  ***",0
	d byte "***                                          TRANSCRIPT                                                              ***",0
	cc byte "***     Items            Rupees                                                                                      ***",0
	e byte "***      Sugar            380                                                                                        ***",0
	f byte "***      Ketch            200                                                                                        ***",0
	g byte "***      Jam              100                     	                                                             ***",0
	h byte "***      Floor            340                                                                                        ***",0
	f1 byte "***      Pine Apple       550                                                                                        ***",0
	f2 byte "***      Cake             600                                                                                        ***",0
	f3 byte "***      Nesvita          125                                                                                        ***",0
	f4 byte "***      Maza Juice       235                                                                                        ***",0
	f5 byte "***      Pickles          650                                                                                        ***",0
	li byte "         -----------------------                                                                                        ",0
	f6 byte "         Customer bil = ",0
	z dword 3180
.code

	main proc
	
	mov edx,offset a
	call writestring
	call writestring
	mov edx,offset b
	call writestring
	mov edx,offset d
	call writestring
	mov edx,offset cc
	call writestring
	mov edx,offset e
	call writestring
	mov edx,offset f
	call writestring
	mov edx,offset g
	call writestring
	mov edx,offset h
	call writestring
	mov edx,offset f1
	call writestring
	mov edx,offset f2
	call writestring
	mov edx,offset f3
	call writestring
	mov edx,offset f4
	call writestring
	mov edx,offset f5
	call writestring
	mov edx,offset li
	call crlf
	call writestring
	mov edx,offset f6
	call writestring
	mov eax,z
	call writedec
	call crlf
	mov edx,offset a
	call writestring
	call writestring
	call crlf
	main endp
	end main
exit