include irvine32.inc

.data

	aa byte "********************************************************************************************************************** ",0
	bb byte " ***                                                                                                               **** ",0
	d byte  " ***                                         WELCOME TO THE SMART SHOP                                             **** ",0
	cc byte " ***                                                PRICE LIST                                                     **** ",0
	de byte " ***               ---------------------------------------------------------------------------                     **** ",0
	r1 byte " ***               | 1.  Sugar      --- 380 | 2.  Lotion    --- 200   |  3. Jam      --- 100 |                     **** ",0
	r2 byte " ***               | 4.  Floor      --- 380 | 5.  Oil       --- 200   |  6. Cream    --- 100 |                     **** ",0
	r3 byte " ***               | 7.  Tea        --- 380 | 8.  Ghee      --- 200   |  9. Beans    --- 100 |                     **** ",0
	r4 byte " ***               | 10. Rice       --- 380 | 11. Noodles   --- 200   | 12. Biscuits --- 100 |                     **** ",0
	r5 byte " ***               | 13. Butter     --- 380 | 14. Apples    --- 200   | 15. Cake     --- 100 |                     **** ",0
	r6 byte " ***               | 16. Milk(1ltr) --- 380 | 17. Banana    --- 200   | 18. Meat     --- 100 |                     **** ",0
	r7 byte " ***               | 19. Juice      --- 380 | 20. Oranges   --- 200   | 21. Sweets   --- 100 |                     **** ",0
	r8 byte " ***               | 22. Jam        --- 380 | 23. Mangows   --- 200   | 24. Pears    --- 100 |                     **** ",0
	r9 byte " ***               | 25. Soap       --- 380 | 26. Bread     --- 200   | 27. Bananas  --- 100 |                     **** ",0

	
	
.code

	main proc
	
	call crlf 
	call crlf
	
	mov edx,0 
	mov edx, offset aa
	call writestring
	
	mov edx,0 
	mov edx, offset bb
	call writestring
	
	mov edx,0 
	mov edx, offset d
	call writestring

	
	mov edx,0 
	mov edx, offset bb
	call writestring
	
	mov edx,0 
	mov edx, offset cc
	call writestring
	
	mov edx,0 
	mov edx, offset bb
	call writestring
	
	
	mov edx,0 
	mov edx, offset de
	call writestring
	
	
	mov edx,0 
	mov edx, offset r1
	call writestring
	
	mov edx,0 
	mov edx, offset r2
	call writestring
	
	
	mov edx,0 
	mov edx,offset r3
	call writestring
	
		
	mov edx,0 
	mov edx, offset r4
	call writestring
	
	
	mov edx,0 
	mov edx, offset r5
	call writestring
	
	
	
	mov edx,0 
	mov edx, offset r6
	call writestring
	
	
	mov edx,0 
	mov edx, offset r7
	call writestring
	
	
	mov edx,0 
	mov edx, offset r8
	call writestring

		
	mov edx,0 
	mov edx, offset r9
	call writestring
	
	
	
		
	mov edx,0 
	mov edx, offset de
	call writestring
	
	
	mov edx,0 
	mov edx, offset bb
	call writestring
	
	mov edx,0 
	mov edx, offset bb
	call writestring
	
	mov edx,0 
	mov edx, offset bb
	call writestring
	call crlf
	
	mov edx,0 
	mov edx, offset aa
	call writestring
	call crlf
	
	
	
	
	call crlf
	call crlf
	call crlf
	
	main endp
	end main
exit