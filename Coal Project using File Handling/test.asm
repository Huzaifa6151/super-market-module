include irvine32.inc

.data

PlayerName byte 8300 dup(?)
bufSize = ($-PlayerName)

errMsg BYTE "Cannot open file",0dh,0ah,0
filename1     BYTE "pro1.dat",0
filename2     BYTE "Pro2.dat",0
filename3     BYTE "Pro3.dat",0
fileHandle   DWORD ?	; handle to output file
byteCount    DWORD ?    ; number of bytes written
bytesWritten DWORD ?
.code
Read_pro1 PROC
	INVOKE CreateFile,
	  ADDR filename1, GENERIC_READ, DO_NOT_SHARE, NULL,
	  OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0

	mov fileHandle,eax		; save file handle
	.IF eax == INVALID_HANDLE_VALUE
        mov eax, 0
        ret
	.ENDIF

	INVOKE ReadFile,		; read text to file
	    fileHandle,		; file handle
	    ADDR PlayerName,		; PlayerName pointer
	    1000,		; number of bytes to write
	    ADDR byteCount,		; number of bytes reading
	    0		; overlapped execution flag

	INVOKE CloseHandle, fileHandle

	mov esi,byteCount		; insert null terminator
	mov PlayerName[esi],0		; into PlayerName   
    	mov edx,offset PlayerName
    	call WriteString
    	call crlf
    ret
Read_pro1 ENDP

Write_pro1 PROC
    INVOKE CreateFile,
	  ADDR filename1, GENERIC_WRITE, DO_NOT_SHARE, NULL,
	  CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0

	mov fileHandle,eax		; save file handle
	.IF eax == INVALID_HANDLE_VALUE
        mov eax, 0
        ret
	.ENDIF

	INVOKE WriteFile,		; write text to file
	    fileHandle,		; file handle
	    ADDR PlayerName,		; PlayerName pointer
	    bufSize,		; number of bytes to write
	    ADDR bytesWritten,		; number of bytes written
	    0		; overlapped execution flag

	INVOKE CloseHandle, fileHandle
    ret
Write_pro1 ENDP


Read_pro2 PROC
	INVOKE CreateFile,
	  ADDR filename2, GENERIC_READ, DO_NOT_SHARE, NULL,
	  OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0

	mov fileHandle,eax		; save file handle
	.IF eax == INVALID_HANDLE_VALUE
        mov eax, 0
        ret
	.ENDIF

	INVOKE ReadFile,		; read text to file
	    fileHandle,		; file handle
	    ADDR PlayerName,		; PlayerName pointer
	    10000,		; number of bytes to write
	    ADDR byteCount,		; number of bytes reading
	    0		; overlapped execution flag

	INVOKE CloseHandle, fileHandle

	mov esi,byteCount		; insert null terminator
	mov PlayerName[esi],0		; into PlayerName   
    	mov edx,offset PlayerName
    	call WriteString
    	call crlf
    ret
Read_pro2 ENDP

Write_pro2 PROC
    INVOKE CreateFile,
	  ADDR filename2, GENERIC_WRITE, DO_NOT_SHARE, NULL,
	  CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0

	mov fileHandle,eax		; save file handle
	.IF eax == INVALID_HANDLE_VALUE
        mov eax, 0
        ret
	.ENDIF

	INVOKE WriteFile,		; write text to file
	    fileHandle,		; file handle
	    ADDR PlayerName,		; PlayerName pointer
	    bufSize,		; number of bytes to write
	    ADDR bytesWritten,		; number of bytes written
	    0		; overlapped execution flag

	INVOKE CloseHandle, fileHandle
    ret
Write_pro2 ENDP

Read_pro3 PROC
	INVOKE CreateFile,
	  ADDR filename3, GENERIC_READ, DO_NOT_SHARE, NULL,
	  OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0

	mov fileHandle,eax		; save file handle
	.IF eax == INVALID_HANDLE_VALUE
        mov eax, 0
        ret
	.ENDIF

	INVOKE ReadFile,		; read text to file
	    fileHandle,		; file handle
	    ADDR PlayerName,		; PlayerName pointer
	    10000,		; number of bytes to write
	    ADDR byteCount,		; number of bytes reading
	    0		; overlapped execution flag

	INVOKE CloseHandle, fileHandle

	mov esi,byteCount		; insert null terminator
	mov PlayerName[esi],0		; into PlayerName   
    	mov edx,offset PlayerName
    	call WriteString
    	call crlf
    ret
Read_pro3 ENDP

Write_pro3 PROC
    INVOKE CreateFile,
	  ADDR filename3, GENERIC_WRITE, DO_NOT_SHARE, NULL,
	  CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0

	mov fileHandle,eax		; save file handle
	.IF eax == INVALID_HANDLE_VALUE
        mov eax, 0
        ret
	.ENDIF

	INVOKE WriteFile,		; write text to file
	    fileHandle,		; file handle
	    ADDR PlayerName,		; PlayerName pointer
	    bufSize,		; number of bytes to write
	    ADDR bytesWritten,		; number of bytes written
	    0		; overlapped execution flag

	INVOKE CloseHandle, fileHandle
    ret
Write_pro3 ENDP
MAIN proc
	;call Read_pro1
	call Read_pro2
	;call Read_pro3
	;call Write_pro1
	;call Write_pro2
	;call Write_pro3
exit
main endp
end main